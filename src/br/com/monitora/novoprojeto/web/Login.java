package br.com.monitora.novoprojeto.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.monitora.novoprojeto.Usuario;
import br.com.monitora.novoprojeto.dao.UsuarioDAO;

@WebServlet(urlPatterns="/login")
public class Login extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		Usuario usuario = new UsuarioDAO().buscaPorEmailESenha(email, password);
		
		PrintWriter writer = resp.getWriter();
		
		if (usuario == null) {
			
			writer.println("<html><body>Usu�rio n�o encontrado!</body></html>");
			
		} else {
			
			Cookie cookie = new Cookie("usuario.logado", usuario.getEmail());	
			cookie.setMaxAge(600);
			resp.addCookie(cookie);
			writer.println("Usu�rio logado: " + usuario.getEmail());
			
		}
		
	}

}
