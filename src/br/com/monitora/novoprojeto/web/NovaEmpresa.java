package br.com.monitora.novoprojeto.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.monitora.novoprojeto.Empresa;
import br.com.monitora.novoprojeto.dao.EmpresaDAO;

@WebServlet(urlPatterns="/novaempresa")
public class NovaEmpresa extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String nome = req.getParameter("nome");
		
		Empresa empresa = new Empresa(nome);
		
		new EmpresaDAO().adiciona(empresa);
		
		PrintWriter writer = resp.getWriter();
		
		writer.println("<html><body>Empresa adicionada com sucesso: " + empresa.getNome() + "</body></html>");
		
	}

}
