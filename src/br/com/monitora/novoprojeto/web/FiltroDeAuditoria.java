package br.com.monitora.novoprojeto.web;

import java.awt.image.RescaleOp;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import br.com.monitora.novoprojeto.Usuario;

@WebFilter(urlPatterns="/*")
public class FiltroDeAuditoria implements Filter {

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
		String uri = request.getRequestURI();
		
		String usuario = getUsuario(request);
		
		System.out.println("Usu�rio " + usuario + " acessando a URI " + uri);
		
		chain.doFilter(req, resp);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	
	private String getUsuario (HttpServletRequest req) {
		
		String usuario = "<deslogado>";
		
		Cookie cookie = new Cookies(req.getCookies()).buscarUsuarioLogado();
		
		if (cookie == null) {
			
			return "<deslogado>";
			
		}
		
		return cookie.getValue();
		
	}
	
}
