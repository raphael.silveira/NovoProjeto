package br.com.monitora.novoprojeto.dao;

import java.util.HashMap;

import com.sun.javafx.collections.MappingChange.Map;

import br.com.monitora.novoprojeto.Usuario;

public class UsuarioDAO {
	
	private final static HashMap<String, Usuario> USUARIOS = new HashMap<>();
	static {
		USUARIOS.put("guilherme.silveira@alura.com.br", new Usuario("guilherme.silveira@alura.com.br","silveira"));
		USUARIOS.put("rodrigo.turini@alura.com.br", new Usuario("rodrigo.turini@alura.com.br","turini"));
		USUARIOS.put("raphajts@gmail.com", new Usuario("raphajts@gmail.com", "rafa1993"));
	}

	public Usuario buscaPorEmailESenha(String email, String senha) {
		if (!USUARIOS.containsKey(email))
			return null;

		Usuario usuario = USUARIOS.get(email);
		if (usuario.getSenha().equals(senha))
			return usuario;
		
		return null;
	}

}
