package br.com.monitora.novoprojeto;

public class Empresa {

	private Long id = 0l;
	private String nome;

	public Empresa(String nome) {
		this.nome = nome;
	}

	public long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setId(Long id) {
		this.id = id;
		
	}

}
